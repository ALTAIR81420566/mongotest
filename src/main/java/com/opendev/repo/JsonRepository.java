package com.opendev.repo;

import org.json.JSONObject;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by altair on 15.03.17.
 */
@Repository
public interface JsonRepository extends MongoRepository<JSONObject, String> {

}
