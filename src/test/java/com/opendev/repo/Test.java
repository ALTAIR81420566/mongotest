package com.opendev.repo;

import org.json.JSONException;
import org.json.JSONObject;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by altair on 15.03.17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class Test{

   @Autowired
    private JsonRepository repository;


    private final static String JSON_DATA =
            "{"
                    + "      \"id\": \"1\","
                    + "      \"name\": \"Julie Sherman\","
                    + "      \"gender\" : \"female\""

                    + "}";


    @org.junit.Test
    public void testShouldSaveJSON() {
        repository.deleteAll();

        JSONObject json = new JSONObject(JSON_DATA);
        try {

            repository.save(json);

            for (JSONObject jsonObject : repository.findAll()) {

                Assert.assertEquals(jsonObject.getInt("id"), 1);
                Assert.assertEquals(jsonObject.getString("gender"), "female");
                Assert.assertEquals(jsonObject.getString("name"), "Julie Sherman");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
